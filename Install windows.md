# Installing for windows.

I've send the dependencies in a folder (vmime_deps) with the code, but you might want to redownload them from the web so I'm adding links to those as well.

- Download libiconv we need two folders for dlls and includes (dlls https://mlocati.github.io/articles/gettext-iconv-windows.html, includ https://ftp.gnu.org/pub/gnu/libiconv/libiconv-1.16.tar.gz)
- Download OpenSSL binaries from https://curl.se/windows/ 
- Open the folder with CMake-gui
- Set VMIME_TLS_SUPPORT_LIB to openssl
- Add the openssl include (OPENSSL_INCLUDE_DIR) 
- Add the OPENSSL_ROOT_DIR entry and point it to your openssl root folder in cmake
- Point the ICONV_INCLUDE_DIR to the include folder inside libiconv-1.16
- Point the ICONV_LIBRARIES (last one should point to libiconv-2.dll in libiconv-1.16-lib)
- Uncheck the following entries in cmake-gui (or use -D if you're using the console)
  - VMIME_HAVE_MLANG_H 
  - VMIME_HAVE_MLANG_LIB
- Uncheck VMIME_HAVE_SASL_SUPPORT
- Uncheck VMIME_HAVE_MESSAGING_PROTO_SENDMAIL
- Check VMIME_BUILD_SAMPLES
- Set CMAKE_BUILD_TYPE to "Debug"

- Compile, it should compile without problems
- Copy the DLLs, that is libiconv-2.dll and all the two dlls inside openssl folder 
  (libcrypto-1_1-x64.dll and libssl-1_1-x64.dll) in the examples folder build\examples\build\bin)

Now you should be able to debug the examples.